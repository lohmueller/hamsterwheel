# hamsterwheel

Monitor your hamsters daily/nightly activity on a Raspberry Pi.

## Features

*  Monitor a GPIO pin for magnet contact (reed switch or hall-effect sensor)
*  Report state to MQTT
   * Includes duration
   * Number of turns
   * Average and maximum speed

## Installation
Installation is quite easy but requires some hardware for monitoring the hamster
wheel.

### Hardware
Add a sensor to GPIO 17. It works fine with a generic hall-effect sensor but a
reed switch should work too.

### Software
Checkout git repository to `/home/pi/hamsterheel`. Other paths are possible too
but it requires changes in the service-file (see below).

Copy `config.sample` to `config.yaml` and change all values to your own values.

### Service
Copy the included `hammy_wheel.service` to `/etc/systemd/system/` and register
the service using `systemd enable hammy_wheel` and `systemd start hammy_wheel`.