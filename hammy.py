#!/usr/bin/python3 -u
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
import time
import logging
import json
import yaml
from threading import Condition, Thread


## Configuration
config = yaml.safe_load(open("config.yaml"))


## HamsterWheel Class for all the "Business Logic"
# Register your callback_handler using `on_change` and make sure
# to call `contact()` every time the wheel turns
class HamsterWheel:
    
    def __init__(self):
        self.start = 0          # Timestamp of first contact
        self.last = 0           # Timestamp of last contact
        self.count = 0          # Number of revolutions
        self.fastest = 0        # Time for fastest revolution [ms]
        self.update_required = False # If on_change needs to report an update
        self.update_sent = 0    # When was last update sent
        self.con = Condition()  # Condition Variable to protect shared state from interrupt handler
                                # and be able to notify() to main thread.
    def on_change(self, callback):
        self.on_change = callback

    def contact(self):
        with self.con:
            now = int(round(time.time() * 1000))
            if self.start == 0:
                logger.debug("First contact detected!")
                self.start = now
            else:
                self.count += 1
                # Make sure we send updates when running started and all few seconds afterwards
                if self.on_change and self.count == 1:
                    self.update_required = True
                    self.update_sent = now
                if now - self.update_sent > 5000:
                    self.update_required = True
                    self.update_sent = now
                logger.debug("Contact detected! Now %d full rotations", self.count)
                diff = now - self.last
                if self.fastest == 0 or diff < self.fastest:
                    logger.debug("New fastest lap with %d ms", diff)
                    self.fastest = diff

            self.last = now
            self.con.notify()

    def _notify_on_change(self, running):
        logger.info("Calling on_change")
        if self.on_change:
            self.on_change({
                "is_running": running,
                "duration": (self.last - self.start)/1000,
                "rotations": self.count,
                "avgspeed": round(self.count*config['wheel']['distance'] / ((self.last-self.start)/1000), 2),
                "topspeed": round(config['wheel']['distance'] / (self.fastest/1000), 2)
            })

    def loop_forever(self):
        while True:
            with self.con:

                # Just pass when interrupt occures, we wait for timeout
                while self.con.wait(timeout=config['wheel']['pin']) != False:
                    if self.update_required:
                        self.update_required = False
                        self._notify_on_change(True)
                    pass

                # Timeout happened - so time to update stats if there was a run
                if self.start != 0:

                    # Check if we have a "run" (more than a single contact)
                    if self.count != 0:
                        avgspeed = self.count*config['wheel']['distance'] / ((self.last-self.start)/1000)
                        topspeed = config['wheel']['distance'] / (self.fastest/1000)
                        logger.info("Start: %d / Last: %d / Duration: %d", self.start, self.last, self.last-self.start)
                        logger.info("Count: %d / Fastest[ms]: %d", self.count, self.fastest);
                        logger.info("Did some running! %d rotations in %.1f seconds speed average/top %.2f/%.2f cm/s", self.count, (self.last-self.start)/1000, avgspeed, topspeed)
                        self._notify_on_change(False)
                    else:
                        logger.info("Aborting run as there was only one contact.")

                    # Reset all stats for next run
                    logger.debug("Resetting wheel.")
                    self.start = 0
                    self.last = 0
                    self.count = 0
                    self.fastest = 0


## Startup Environment
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logging.info("Starting up...")
wheel = HamsterWheel()


## Startup Hardware
GPIO.setmode(GPIO.BOARD)

GPIO.setup(config['wheel']['pin'], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(config['wheel']['pin'], GPIO.FALLING, callback=(lambda channel: wheel.contact()), bouncetime = 200)


## Startup MQTT
def mqtt_on_connect(client, userdata, flags, rc):
    logger.info("MQTT: Connected with result code "+str(rc))
    mqttc.publish(MQTT_BASE + "/status", "online", retain=True)

logger.info("MQTT: Connecting to '%s'", config['mqtt']['server'])
mqttc = mqtt.Client(client_id = config['mqtt']['client_id'], clean_session=False)
mqttc.username_pw_set(config['mqtt']['username'], config['mqtt']['password'])
mqttc.on_connect = mqtt_on_connect
mqttc.will_set(config['mqtt']['base'] + "/status", "offline", retain=True)
mqttc.connect(config['mqtt']['server'], config['mqtt']['port'])
mqttc.loop_start()


# Connect modules
wheel.on_change(lambda stats: mqttc.publish(config['mqtt']['base'] + "/current", json.dumps(stats)))


## Main Application loop
wheel.loop_forever()
